package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	// internal
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"
	. "codeberg.org/zhivtone/lib/types/rpc-types"

	// stdlib
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"time"
)

type Users struct{}

func (*Users) Register(args UsersRegisterArgs) (UsersRegisterReply, error) {
	reply := UsersRegisterReply{}
	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	if conf.Settings.CloseRegistration {
		return reply, Error{
			Code:    ErrorCodeOther,
			Message: "registration is closed",
		}
	}

	if !isLoginAllowed(args.Login) {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "login should contain only [a-zA-Z0-9\\-_] (regexp)",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "invalid encoding of pass_b64",
		}
	}

	if len(pass) < 8 {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "password length should be greater than or equal 8 symbols",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c != 0 {
		return reply, Error{
			Code:    ErrorCodeOther,
			Message: "login is already taken",
		}
	}

	var uid = primitive.NewObjectID()

	if _, err = dbUsers.InsertOne(
		helpers.GetCtx(),
		bson.M{
			"_id":       uid,
			"login":     args.Login,
			"pass_hash": passHash,
			"last_beat": time.Now(),
			"actions":   bson.A{},
		},
	); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	reply.UID = uid.Hex()

	return reply, nil
}

func (*Users) Get(args UsersGetArgs) (UsersGetReply, error) {
	reply := UsersGetReply{}

	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "invalid encoding of pass_b64",
		}
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c == 0 {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "user with this login not found",
		}
	}

	var user types.User
	if err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	).Decode(&user); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if user.PasswordHash != passHash {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "wrong password",
		}
	}

	reply = UsersGetReply{
		UUID:     user.UUID.Hex(),
		LastBeat: user.LastBeat,
		Actions:  make([]types.AutoAction, len(user.Actions)),
	}

	for i, act := range user.Actions {
		reply.Actions[i] = types.AutoAction{
			After:      act.After,
			Parameters: act.Parameters,
			Executor:   act.Executor,
			Executed:   act.Executed,
		}
	}

	return reply, nil
}

// Sorry for naming, but this jsonrpc uses _ as delimiter
func (*Users) Mark_alive(args UsersMarkAliveArgs) (UsersMarkAliveReply, error) {
	reply := UsersMarkAliveReply{}

	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "invalid encoding of pass_b64",
		}
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c == 0 {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "user with this login not found",
		}
	}

	var user types.User

	if err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	).Decode(&user); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "wrong password",
		}
	}

	var now = time.Now().UTC()

	if _, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
		bson.M{
			"$set": bson.M{
				"last_beat":            now,
				"actions.$[].executed": false,
			},
		},
	); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	reply.SetTime = now

	return reply, nil
}
