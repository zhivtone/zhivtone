package main

import (
	// internal
	"codeberg.org/zhivtone/lib/helpers"

	// stdlib
	"encoding/base64"
	"strings"
	"time"
)

const (
	ErrNoAuthType          = helpers.Error("no auth type")
	ErrUnsupportedAuthType = helpers.Error("unsupported auth type")
	ErrInvalidBase64       = helpers.Error("invalid base64")
	ErrNotFoundColon       = helpers.Error("colon not found")
)

func decryptCreds(orig string) (string, string, error) {
	authArr := strings.Fields(orig)
	if len(authArr) < 2 {
		return "", "", ErrNoAuthType
	}
	authType, encAuthCred := authArr[0], authArr[1]
	if authType != "Basic" {
		return "", "", ErrUnsupportedAuthType
	}
	authCredBytes, err := base64.StdEncoding.DecodeString(encAuthCred)
	if err != nil {
		return "", "", ErrInvalidBase64
	}
	login, password, found := strings.Cut(string(authCredBytes), ":")
	if !found {
		return "", "", ErrNotFoundColon
	}
	return login, password, nil
}

// [a-zA-Z0-9\-_]
var allowedLoginSymbols = [...]rune{
	'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u',
	'v', 'w', 'x', 'y', 'z',
	'A', 'B', 'C', 'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U',
	'V', 'W', 'X', 'Y', 'Z',
	'0', '1', '2', '3', '4', '5', '6',
	'7', '8', '9', '_', '-',
}

func isLoginAllowed(login string) bool {
LOGIN_FOR:
	for _, s := range login {
		for _, a := range allowedLoginSymbols {
			if s == a {
				continue LOGIN_FOR
			}
		}
		return false
	}
	return true
}

func isDurationAllowed(dur helpers.Duration) bool {
	d := time.Duration(dur)
	return int(d) > 0 && int64(d.Hours())%12 == 0 && int64(d.Minutes())%60 == 0
}
