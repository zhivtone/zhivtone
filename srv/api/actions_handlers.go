package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	// internal
	"codeberg.org/zhivtone/lib/execs"
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"
	. "codeberg.org/zhivtone/lib/types/rpc-types"

	// stdlib
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"time"
)

type Actions struct{}

func (*Actions) Types_get() (ActionsGetTypesReply, error) {
	reply := ActionsGetTypesReply{}

	if execs.ActionExecutors == nil {
		return reply, nil
	}

	reply.Types = make(
		[]execs.ExecutorType,
		len(execs.ActionExecutors),
	)

	var i int = 0
	for t, _ := range execs.ActionExecutors {
		reply.Types[i] = t
		i++
	}

	return reply, nil

}

func (*Actions) Add(args ActionsAddArgs) (ActionsAddReply, error) {
	reply := ActionsAddReply{}

	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	if args.NewActions == nil || len(args.NewActions) == 0 {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty new_actions list",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "invalid encoding of pass_b64",
		}
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c == 0 {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "user with this login not found",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	var user types.User

	if err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	).Decode(&user); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}

	}

	if user.PasswordHash != passHash {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "wrong password",
		}
	}

	var push = bson.A{}

	for i, act := range args.NewActions {
		if act.After == helpers.Duration(time.Duration(0)) {
			return reply, Error{
				Code:    ErrorCodeEmptyArgs,
				Message: "Empty `after` field",
				Data: ActionsAddErrData{
					ActionIdx: i,
				},
			}
		} else if !isDurationAllowed(act.After) {
			return reply, Error{
				Code:    ErrorCodeInvalidArgs,
				Message: "Invalid `after` field value",
				Data: ActionsAddErrData{
					ActionIdx: i,
				},
			}
		}

		if ex, ok := execs.ActionExecutors[act.Executor]; !ok {

		} else if valid, errText := ex.CheckValid(
			act.Parameters,
		); !valid {
			return reply, Error{
				Code:    ErrorCodeInvalidArgs,
				Message: "invalid params",
				Data: ActionsAddErrData{
					ActionIdx:      i,
					ParamErrorText: errText,
				},
			}
		}

		push = append(
			push,
			bson.M{
				"after":     act.After,
				"params":    act.Parameters,
				"exec_type": act.Executor,
			},
		)
	}

	if _, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
		bson.M{
			"$push": bson.M{
				"actions": bson.M{
					"$each": push,
				},
			},
		},
	); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	reply.ActionsAdded = len(push)
	return reply, nil
}

func (*Actions) Delete(args ActionsDeleteArgs) (ActionsDeleteReply, error) {
	reply := ActionsDeleteReply{}

	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	if args.Indexes == nil || len(args.Indexes) == 0 {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty indexes list",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "invalid encoding of pass_b64",
		}
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c == 0 {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "user with this login not found",
		}
	}

	var user types.User

	if err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	).Decode(&user); err != nil {
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "wrong password",
		}
	}

	// map[int]struct{} because it's
	// simple solution for lookup
	var indexes = map[int]struct{}{}

	// haha!
	for idxidx, idx := range args.Indexes {
		if idx > len(user.Actions)-1 {
			return reply, Error{
				Code:    ErrorCodeInvalidArgs,
				Message: "index out of range",
				Data: ActionsDeleteErrData{
					InvalidIdxIdx: idxidx,
				},
			}
		}
		indexes[idxidx] = struct{}{}
	}

	var resultActions = make([]types.AutoAction, 0)

	for i := range user.Actions {
		if _, ok := indexes[i]; !ok {
			resultActions = append(resultActions, user.Actions[i])
		}
	}

	if _, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
		bson.M{
			"$set": bson.M{
				"actions": resultActions,
			},
		},
	); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	reply.ActionsDeleted = len(resultActions)

	return reply, nil
}

func (*Actions) Edit(args ActionsEditArgs) (ActionsEditReply, error) {
	reply := ActionsEditReply{}

	if args.Login == "" || args.PasswordB64 == "" {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty login or pass_b64",
		}
	}

	if args.ChangedActions == nil || len(args.ChangedActions) == 0 {
		return reply, Error{
			Code:    ErrorCodeEmptyArgs,
			Message: "empty changed_actions map",
		}
	}

	pass, err := base64.StdEncoding.DecodeString(args.PasswordB64)
	if err != nil {
		return reply, Error{
			Code:    ErrorCodeInvalidArgs,
			Message: "wrong encoding of pass_b64",
		}
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	)
	if err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	if c == 0 {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "users with this login not found",
		}
	}

	var user types.User

	if err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
	).Decode(&user); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		return reply, Error{
			Code:    ErrorCodeWrongAuth,
			Message: "wrong password",
		}
	}

	// MongoDB has no method to work with
	// array elements by items, so we take
	// all tasks and operate with them
	// by ourselves
	var resultActions = user.Actions

	for idx, act := range args.ChangedActions {
		if idx > len(resultActions)-1 {
			return reply, Error{
				Code:    ErrorCodeInvalidArgs,
				Message: "index out of range",
				Data: ActionsEditErrData{
					ActionIdx: idx,
				},
			}
		}

		// remind: this is like mongo's $set
		if act.After != helpers.Duration(time.Duration(0)) {
			if !isDurationAllowed(act.After) {
				return reply, Error{
					Code:    ErrorCodeInvalidArgs,
					Message: "invalid `after` field value",
					Data: ActionsEditErrData{
						ActionIdx: idx,
					},
				}
			}
			resultActions[idx].After = act.After
		}

		if act.Parameters != nil {
			if ex, ok := execs.ActionExecutors[act.Executor]; ok {
				valid, errText := ex.CheckValid(act.Parameters)
				if !valid {
					return reply, Error{
						Code:    ErrorCodeInvalidArgs,
						Message: "invalid params",
						Data: ActionsEditErrData{
							ActionIdx:      idx,
							ParamErrorText: errText,
						},
					}
				}

				resultActions[idx].Parameters = act.Parameters
			}
		}

		if act.Executor != execs.ExecTypeEmpty {
			resultActions[idx].Executor = act.Executor
		}
	}

	if _, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{
				Pattern: "^" + args.Login + "$",
				Options: "i",
			},
		},
		bson.M{
			"$set": bson.M{
				"actions": resultActions,
			},
		},
	); err != nil {
		errLog.Printf("Error accessing to MongoDB: %v\n", err)
		return reply, Error{
			Code:    ErrorCodeInternal,
			Message: "internal server error",
		}
	}

	reply.ActionsEdited = len(args.ChangedActions)

	return reply, nil
}
