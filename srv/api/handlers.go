package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	// internal
	"codeberg.org/zhivtone/lib/execs"
	_ "codeberg.org/zhivtone/lib/execs/default_modules"
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"

	// stdlib
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	if r.URL.Path != "/" && r.URL.Path != "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found"}`))
		return
	}

	w.Header().Add("Location", "/ping")
	w.WriteHeader(301)
}

func pingHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write([]byte(`{"msg":"Pong"}`))
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if r.Method != http.MethodPost {
		w.WriteHeader(405)
		w.Write([]byte(`{"error":"Method not allowed"}`))
		return
	}

	if conf.Settings.CloseRegistration {
		w.WriteHeader(403)
		w.Write([]byte(`{"error":"Forbidden","msg":"registration is closed"}`))
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(415)
		w.Write([]byte(`{"error":"Unsupported Media Type"}`))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error reading body: %v\n", err)
		return
	}

	var rr types.RegisterRequest
	err = json.Unmarshal(body, &rr)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"invalid json"}`))
		return
	}

	if rr.Login == "" || rr.PasswordB64 == "" {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"empty login or password"}`))
		return
	}

	bs, err := base64.StdEncoding.DecodeString(rr.PasswordB64)

	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"invalid base64"}`))
		return
	}

	pass := strings.TrimSpace(string(bs))

	if len(pass) < 8 {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"password is too short"}`))
		return
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if !isLoginAllowed(rr.Login) {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"login should contain only [a-zA-Z0-9\-_]"}`))
		return
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + rr.Login + "$", Options: "i"},
		},
	)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error accessing to db: %v\n", err)
		return
	}

	if c != 0 {
		w.WriteHeader(409)
		w.Write([]byte(`{"error":"Conflict","msg":"login is already taken"}`))
		return
	}

	var (
		uid = primitive.NewObjectID()
	)

	_, err = dbUsers.InsertOne(
		helpers.GetCtx(),
		bson.M{
			"_id":       uid,
			"login":     rr.Login,
			"pass_hash": passHash,
			"settings": bson.M{
				"email":   "",
				"actions": bson.A{},
			},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error accessing to db: %v\n", err)
		return
	}

	w.WriteHeader(201)

	w.Write([]byte(`{"msg":"OK","uid":"` + uid.Hex() + `"}`))
}

func getUserHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	login, pass, err := decryptCreds(r.Header.Get("Authorization"))
	if err != nil {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}
	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error accessing to db: %v\n", err)
		return
	}
	if c == 0 {
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found","msg":"users with this login not found"}`))
		return
	}

	var user types.User
	err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	).Decode(&user)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error accessing to db: %v\n", err)
		return
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)
	if user.PasswordHash != passHash {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"wrong password"}`))
		return
	}
	var res = types.GetUserResponse{
		Message:  "OK",
		UUID:     user.UUID.Hex(),
		Login:    user.Login,
		LastBeat: user.LastBeat,
		Settings: types.GetUserResponseSettings{},
	}
	res.Settings.Actions = make([]types.AutoAction, len(user.Actions))
	for i, act := range user.Actions {
		res.Settings.Actions[i] = types.AutoAction{
			After:      act.After,
			Parameters: act.Parameters,
			Executor:   act.Executor,
			Executed:   act.Executed,
		}
	}
	encRes, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("Error encoding json: %v\n", err)
		return
	}
	w.WriteHeader(200)
	w.Write(encRes)
}

// sorry for name, i can't
// think out better name
func aliveHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if r.Method != http.MethodPost {
		w.WriteHeader(405)
		w.Write([]byte(`{"error":"Method Not Allowed"}`))
		return
	}

	login, pass, err := decryptCreds(r.Header.Get("Authorization"))
	if err != nil {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	if c == 0 {
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found","msg":"user by login not found"}`))
		return
	}

	var user types.User

	err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	).Decode(&user)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"wrong password"}`))
		return
	}

	var now = time.Now()

	_, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
		bson.M{
			"$set": bson.M{
				"last_beat":                     now,
				"settings.actions.$[].executed": false,
			},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	nowEnc, err := now.MarshalJSON()

	if err != nil {
		nowEnc = []byte(`""`)
		errLog.Printf("error marshalling time (?): %v\n", err)
	}

	w.WriteHeader(200)
	w.Write([]byte(`{"msg":"OK","set_time":` + string(nowEnc) + `}`))
}

func typesHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if execs.ActionExecutors != nil {
		var ts = make([]execs.ExecutorType, len(execs.ActionExecutors)-1)
		i := 0
		for t, _ := range execs.ActionExecutors {
			ts[i] = t
		}

		b, err := json.Marshal(ts)
		if err != nil {
			errLog.Printf("error marshalling json: %v\n", err)
			w.WriteHeader(500)
			w.Write([]byte(`{"error":"Internal Server Error"}`))
			return
		}
		w.WriteHeader(200)
		w.Write([]byte(`{"result":`))
		w.Write(b)
		w.Write([]byte(`}`))
		return
	}

	w.WriteHeader(404)
	w.Write([]byte(`{}`))
}

func settingsHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	b, err := json.Marshal(conf.Settings)
	if err != nil {
		errLog.Printf("error marshalling json: %v\n", err)
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		return
	}

	w.WriteHeader(200)
	w.Write([]byte(`{"result":`))
	w.Write(b)
	w.Write([]byte(`}`))
}
