package main

import (
	// external
	"go.mongodb.org/mongo-driver/mongo"
	monOpts "go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	// internal
	"codeberg.org/zhivtone/lib/helpers"

	// stdlib
	"flag"
	"os"
)

func init() {
	// flag initializing
	flag.StringVar(&flagConfigPath, "config", "./config.toml", "path to config file")
	flag.BoolVar(&flagWriteDebug, "debug", false, "directs debug log to output (stderr")
	flag.Parse()

	// debug log directing
	if flagWriteDebug {
		debugLog.SetOutput(os.Stderr)
	}

	// config initializing
	{
		file, err := os.Open(flagConfigPath)
		if err != nil {
			errLog.Printf("Error reading config: %v\n", err)
			os.Exit(1)
		}
		err = conf.InitFromTOML(file)
		if err != nil {
			errLog.Printf("Error parsing TOML: %v\n", err)
			os.Exit(1)
		}
	}

	// database initializing
	{
		var err error
		dbClient, err = mongo.Connect(helpers.GetCtx(), monOpts.Client().ApplyURI(conf.DatabaseURL))
		if err != nil {
			errLog.Printf("Error connecting db: %v\n", err)
			os.Exit(1)
		}

		err = dbClient.Ping(helpers.GetCtx(), readpref.Primary())
		if err != nil {
			errLog.Printf("Error pinging db: %v\n", err)
			os.Exit(1)
		}

		db = dbClient.Database("main")
		dbUsers = db.Collection("users")
	}
}
