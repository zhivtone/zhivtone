package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	// internal
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"
	"codeberg.org/zhivtone/zhivtone/internal/config"

	"codeberg.org/zhivtone/lib/execs"
	_ "codeberg.org/zhivtone/lib/execs/default_modules"

	// stdlib
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

// config
var conf config.Config

// database vatiables
var (
	dbClient *mongo.Client
	db       *mongo.Database
	dbUsers  *mongo.Collection
)

// logs
var (
	infoLog  *log.Logger = log.New(os.Stdout, " [  INF  ] ", log.Ldate|log.Ltime)
	errLog   *log.Logger = log.New(os.Stderr, " [ ERROR ] ", log.Ldate|log.Ltime|log.Llongfile)
	debugLog *log.Logger = log.New(io.Discard, " [ DEBUG ] ", log.Ldate|log.Ltime|log.Lshortfile)
)

// flag values
var (
	flagConfigPath string
	flagWriteDebug bool
)

func main() {
	os.Exit(mainWithCode())
}

// because of defer
func mainWithCode() int {
	if dbClient != nil {
		defer func() {
			dbClient.Disconnect(helpers.GetCtx())
		}()
	}

	body, err := ioutil.ReadAll(os.Stdin)
	if err != nil && err.Error() != "EOF" {
		errLog.Printf("Error reading stdin: %v\n", err)
		return 1
	}

	var execIdxs map[string][]int

	err = json.Unmarshal(body, &execIdxs)
	if err != nil {
		errLog.Printf("Error encoding json: %v\n", err)
		return 1
	}

	var (
		user     types.User
		wg       sync.WaitGroup
		userActs = make(map[string][]types.AutoAction)
	)

	for userLogin, idxs := range execIdxs {
		err = dbUsers.FindOne(
			helpers.GetCtx(),
			bson.M{"login": userLogin},
		).Decode(&user)
		if err != nil {
			errLog.Printf("Error accessing to db: %v\n", err)
			continue
		}

		userActs[userLogin] = user.Actions

	ACTIONS_FOR:
		for _, actI := range idxs {
			if actI > len(user.Actions)-1 {
				errLog.Printf("%s: action index (%d) > len(actions) - 1 (%d)\n", userLogin, actI, len(user.Actions)-1)
				continue
			}

			for t, ex := range execs.ActionExecutors {
				if user.Actions[actI].Executor == t {
					wg.Add(1)
					go func(act types.AutoAction, login string, i int) {
						err := ex.Run(user.Actions[actI].Parameters)
						if err != nil {
							errLog.Printf("%s: %d: error executing: %v\n", login, i, err)
						} else {
							infoLog.Printf("%s: %d: executed successfully\n", login, i)
							userActs[login][i].Executed = true
						}
						wg.Done()
					}(user.Actions[actI], userLogin, actI)

					continue ACTIONS_FOR
				}
			}

			errLog.Printf("%s: %d: unknown exec_type %s\n", userLogin, actI, user.Actions[actI].Executor)
		}
	}

	wg.Wait()
	if len(userActs) > 0 {
		var opers = make([]mongo.WriteModel, 0)

		for login, acts := range userActs {
			op := mongo.NewUpdateOneModel()
			op.SetFilter(bson.M{
				"login": login,
			})
			op.SetUpdate(bson.M{
				"$set": bson.M{
					"settings.actions": acts,
				},
			})
			opers = append(opers, op)
		}

		_, err = dbUsers.BulkWrite(
			helpers.GetCtx(),
			opers,
			nil,
		)

		if err != nil {
			errLog.Printf("Error setting exec status of actions: %v\n", err)
			return 1
		}

		infoLog.Printf("Successfully set exec statuses!\n")
	}

	return 0
}
