package config

import (
	"github.com/BurntSushi/toml"
	"io"
)

// InitFromToml wraps toml.DecodeReader,
// because "you don't need to import toml to use
// config"(c)
func (c *Config) InitFromTOML(stream io.Reader) error {
	_, err := toml.DecodeReader(stream, c)
	return err
}
