CC=gcc
CXX=g++
GOC=go
GOFLAGS=
GO_LDFLAGS=
GOTAGS=
BUILDDIR=build
BINEXT=

all: api actions_checker executor

api:
	$(GOC) build -ldflags="$(GO_LDFLAGS)" -tags="$(GOTAGS)" -o="$(BUILDDIR)/api$(BINEXT)" $(GOFLAGS) ./srv/api

actions_checker:
	$(GOC) build -ldflags="$(GO_LDFLAGS)" -tags="$(GOTAGS)" -o="$(BUILDDIR)/actions_checker$(BINEXT)" $(GOFLAGS) ./srv/actions_checker


executor:
	$(GOC) build -ldflags="$(GO_LDFLAGS)" -tags="$(GOTAGS)" -o="$(BUILDDIR)/executor$(BINEXT)" $(GOFLAGS) ./srv/executor

clean:
	rm -rf $(BUILDDIR)/api$(BINEXT) $(BUILDDIR)/actions_checker$(BINEXT) $(BUILDDIR)/executor$(BINEXT)
